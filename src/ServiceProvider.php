<?php
namespace KDA\Laravel\Layouts\Seo;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Layouts\Facades\LayoutManager;
use Blade;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    protected $packageName ='laravel-layouts-seo';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
        LayoutManager::registerRenderHook(
            'head.meta',
            function () {
                return Blade::render("@include('kda-seo::meta')");
            }
        );
    }
}
