# Security Policy

## Supported Versions

| Version | Supported |
| ------- | ------------------ |
| 1.x | :white_check_mark: |

## Reporting a Vulnerability

If you discover a security vulnerability, please email Fabien Karsegard via [fabien@karsegard.ch](mailto:fabien@karsegard.ch). All security vulnerabilities will be promptly addressed.